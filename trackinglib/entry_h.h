#ifndef ENTRY_H
#define ENTRY_H
/** @brief A structure defining an entry in a track.
 *
 * This structure contains all necessary details to define a location in space 
 * and time. It is stored as a node in a doubly linked list, and therefore
 * has pointers to the next node and the previous node in the list. The 
 * 'matched' parameter indicates the track ID to which the Entry belongs.
 */
struct Entry_s
{
    double x;       //!< X-position of the tracer.
    double y;       //!< Y-position of the tracer.
    double z;       //!< Z-position of the tracer.
    double t;       //!< Time of the entry.
    unsigned int d; //!< The cluster size of the located tracer.
    int matched;    //!< ID of the track to which the tracer is matched.
    struct Entry_s* next;
    struct Entry_s* prev;
};
typedef struct Entry_s Entry;  

/** @brief Creates a new Entry
 *
 * Creates a new Entry struct using dynamic memory allocation.
 *
 * @param x     The x-position of the Entry.
 * @param y     The y-position of the Entry.
 * @param z     The z-position of the Entry.
 * @param t     The time at which the Entry was located.
 * @param next  A pointer to the next Entry in the linked list.
 * @param prev  A pointer to the previous Entry in the linked list.
 *
 * @return A pointer to the new Entry.
 */
Entry* createEntry(double x, double y, double z, double t, unsigned int d, Entry* next, Entry* prev);
/** @brief Prints Entry details to the terminal. Used mainly for debugging. 
 * 
 * @param entry A pointer to an Entry
 */
void printEntry(Entry* entry);
/** @brief Calculates the square of the Euclidean distance between two points.
 *
 * @param[in] e1 A pointer to the first Entry.
 * @param[in] e2 A pointer to the second Entry.
 *
 * @return A double containing the distance between the two points.
 */
double getSquareDistance(Entry* e1, Entry* e2);
                  
Entry* createEntry(double x, double y, double z, double t, unsigned int d, Entry* next, Entry* prev)
{
    Entry* new_entry = (Entry*)malloc(sizeof(Entry));
    if(new_entry == NULL)
        return 0;
        
    new_entry->x = x;
    new_entry->y = y;
    new_entry->z = z;
    new_entry->t = t;
    new_entry->d = d;
    new_entry->matched = 0;
    new_entry->next = next;
    new_entry->prev = prev;
    
    return new_entry;
} 
        
double getSquareDistance(Entry* e1, Entry* e2)
{
    return (e1->x - e2->x)*(e1->x - e2->x) +
           (e1->y - e2->y)*(e1->y - e2->y) +
           (e1->z - e2->z)*(e1->z - e2->z);       
}             

#endif

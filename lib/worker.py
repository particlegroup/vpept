# -*- coding: utf-8 -*-
import numpy as np
import lofpy
import vmptutils as vuti
from sklearn.cluster import DBSCAN

## @brief Locates the tracers in a frame of LOR's
#
# This function converts a matrix of doubles representing a number of lines
# of response into a Frame object. This Frame is then processed and the tracers
# within the field of view are located.
#
# The following steps are followed to locate the tracers:
#   - Each line is discretized and a Voronoi tessellation is applied to these
#     seed points. For each line, the seed with the smallest Voronoi region is
#     saved and the rest are discarded. This is all performed within the Frame
#     object.
#   - A Local Outlier Factor technique is applied to remove some outliers.
#   - The seeds with Voronoi volumes in the top 50% are discarded.
#   - DBSCAN is used to cluster the remaining data points.
#   - The centroid for each cluster is calculated.
#
# @param frame_data_i A frame of LOR data. This is a NumPy array with the form
#                     (Xa, Ya, Za, Xb, Yb, Zb, t), where (Xa, Ya, Za) and
#                     (Xb, Yb, Zb) are two points which define a line that
#                     was detected at time t.
# @return An \f$N\times 4\f$ NumPy array, where N is the number of tracers
#         detected. The rows of the array take the form (x, y, z, t), where
#         t is the average of the times in frame_data_i and (x, y, z) is the
#         location of the tracer.
def locate(frame_i, settings_in):
    # Create a Frame object from the data
    #frame_i = frame.Frame(frame_data_i)
    if frame_i.getEventRate() < settings_in['min_rate']:
        return np.zeros((0,5))
    # Discretize LOR's and generate Voronoi tessellations to determine the
    #   smallest cell for each LOR.
    poi = frame_i.getPointsOfInterest(settings_in['eps'])
    all_points = frame_i.getPointsAt(poi['ind'])
    all_vols = poi['vol']
    # Get the average time for the frame
    time_i = frame_i.getFrameTime()
    # Perform a Local Outlier Factor analysis on the points of interest
    lof = lofpy.getLOF(settings_in['k'], all_points)
    low_lof = vuti.getLowFraction(lof, settings_in['lof_frac'])
    lof_smoothed = all_points[low_lof,:]
    lof_smoothed_vols = np.array(all_vols)[low_lof]
    # Clean the data further by discarding the points with large Voronoi cells
    low_vol = vuti.getLowFraction(lof_smoothed_vols, settings_in['vol_frac'])
    remainders = lof_smoothed[low_vol,:]
    # Perform DBSCAN clustering
    db = DBSCAN(eps=settings_in['eps'], min_samples=settings_in['k']).fit(remainders)
    labels = db.labels_
    n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
    locations = np.zeros((n_clusters,5))
    # Calculate geometric mean of each cluster
    for cluster in xrange(0, n_clusters):
        cluster_inds = np.where(labels == cluster)[0]
        location = np.sum(remainders[cluster_inds,:], axis=0)/len(cluster_inds)
        locations[cluster,0:3] = location
        locations[cluster,3] = time_i
        locations[cluster,4] = len(cluster_inds)
    #end if
    del frame_i
    del poi
    del all_points
    del all_vols
    del time_i
    del lof
    del low_lof
    del lof_smoothed
    del lof_smoothed_vols
    del low_vol
    del remainders
    del db
    del n_clusters
    del labels
    return locations
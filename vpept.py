# built-in libraries
import numpy as np
import os
import multiprocessing
import time
import datetime
import sys
import math
import gc

from numpy import newaxis
from functools import partial

from lib import frame
from lib import vmptutils as vuti
from lib import trackinglib
from lib import vmptclib
from lib import worker
from lib import writers

## @brief The main method of the VMPT program.

if __name__ == "__main__":
    # Open the configuration file.
    config_path = 'config.ini'
    [settings, file_path, output_folder] = vuti.loadSettings(config_path)

    outcount = 0
    outtemp = output_folder
    while os.path.exists(outtemp):
        outtemp = output_folder + '_' + str(outcount)
        outcount = outcount + 1
        
    output_folder = outtemp
    os.makedirs(output_folder)
    #--------------------------------------------------------------------------
    # Determine the number of cores used for processing.
    # If the number is set to more than the number of threads on the machine,
    # or is equal to -1, set it to the number of threads.
    #--------------------------------------------------------------------------
    if settings['cores'] > multiprocessing.cpu_count:
        print('Number of cores requested greater than physical count. Using maximum.')
        settings['cores']   = multiprocessing.cpu_count
    #end if
    if settings['cores']   == -1:
        print('Using maximum number of cores.')
        settings['cores']   = multiprocessing.cpu_count
    #end if
    frame_size = settings['lpt'] * settings['Nt'] 

    print('Starting location using ' + str(settings['cores']  ) + ' physical cores.')
    print(' ')
    print('==================================================')

    writers.writeLogHeader(settings, output_folder)
    #--------------------------------------------------------------------------
    # Data is loaded from the list mode file in 'batches', with each batch
    # containing a number of 'frames'. A frame consists of a number of lines
    # of response equal to the product of LINES_PER_TRACER and the number of
    # expected tracers. A batch contains READ_SIZE frames, and therefore
    # contains a total number of lines of response equal to
    # \f$READ_SIZE \times LINES_PER_TRACER \times num_tracers\f$.
    #
    # The data is loaded a batch at a time to decrease memory usage. Each batch
    # is split into frames and the list of frames is passed to a multiprocessing
    # Pool object. Once all the frames have been processed, the located tracer
    # positions are written to file. In a future version, the tracer positions
    # will instead be fed into the tracking routine and the tracks will be
    # saved.
    #
    # For optimal processing time, the setting READ_SIZE should be a multiple
    # of NUM_CORES.
    #--------------------------------------------------------------------------
    success = 1
    batch_num = 0

    file_name = os.path.basename(file_path)
    file_size = os.path.getsize(file_path)
    # Determines the index of the line at which the process must start, based
    # on a starting time. Exits if errors are encountered.
    [start_byte, cur_time] = vmptclib.getStartingPoint(file_path, settings['t0']);
    cur_byte = start_byte
    if(cur_byte == -1):
        print('Could not open file: ' + file_name)
        sys.exit()
    if(cur_byte == -2):
        print('Start time exceeds max time.')
        sys.exit()
    # Total bytes to be processed
    total_bytes = file_size - start_byte
    init_time = time.time()
    init_date = datetime.datetime.now()
    pool = multiprocessing.Pool(processes=settings['cores']  )
    max_lines = settings['fpb']*frame_size

    while(batch_num < settings['Nb'] or settings['Nb'] == -1):
        try:
            print(' ')
            print('Loading ' + str(settings['fpb']*frame_size) + ' lines from file ' + file_name)
            # Load the next batch from the list mode file

            [nlines_loaded,cur_byte,cur_time,lines_arr] = vmptclib.getFrameBatch(file_path, cur_byte, cur_time, frame_size, settings['fpb'], settings['window'])
            if(nlines_loaded == -1):
                print('Cannot load file. Exiting program.')
                success = 0;
                break
            if(nlines_loaded == -2):
                print('End of file reached.')
                break
            if(nlines_loaded < frame_size):
                print('End of file reached.')
                break
            if(nlines_loaded < max_lines):
                lines_arr = lines_arr[0:nlines_loaded*7]
            # Reshape the array into an Nx7 matrix.
            lines = np.reshape(lines_arr,(-1,7))
            # Determine the size of the data set.
            num_lines = lines.shape[0]
            num_frames = int(math.ceil(num_lines / float(frame_size)))
            # Split the batch into frames.
            split_indices = range(frame_size, num_frames*frame_size, frame_size)
            split_data = np.split(lines, split_indices)
            pool_inputs = [frame.Frame(split_i) for split_i in split_data]

            print('Data loaded')
            # Initialize the multiprocessing Pool

            t_start = time.time()
            #------------------------------------------------------------------
            # Retrieve the output from the Pool. multiprocessing.Pool is
            # synchronised, so output should be ordered according to the input
            # order. This means that locations will be chronologically
            # ordered.
            #------------------------------------------------------------------
            locate_func = partial(worker.locate, settings_in=settings)
            pool_output = pool.map(locate_func, pool_inputs, chunksize=1)

            t_end = time.time()
            # --- Write outputs to terminal and log file --- #
            batch_num = batch_num + 1
            percent = 100 * max((float(cur_byte - start_byte) / float(total_bytes), float(batch_num)/float(settings['Nb'])))
            eta = init_date + datetime.timedelta(0,float(t_end-init_time)/(percent/100.0))

            str_percent = str(format(percent, '.5f'))
            str_time = str(format(t_end-t_start, '.5f'))
            str_eta = eta.strftime('%I:%M%p on %B %d, %Y\n')

            print('Batch ' + str(batch_num) + ' processed in ' + str_time + 's')
            print(str_percent + '% complete.')
            print('ETA: ' + str_eta)
            located = np.vstack(np.array(pool_output))
            vuti.writeLocationsToFile(output_folder, located)

            writers.writeProcessLog(located.shape[0], str_time, str_percent , str_eta, output_folder)

            # --- Free memory --- #
            # Free small variables
            del nlines_loaded
            del num_lines
            del num_frames
            del t_start
            del t_end
            del percent
            del eta
            del str_percent
            del str_time
            del str_eta
            # Free arrays/matrices
            del split_indices
            del pool_inputs
            del pool_output
            del lines_arr
            del lines
            del located

            gc.collect()
        except KeyboardInterrupt, SystemExit:
            pool.close()
            pool.join()
            print('\n Operation cancelled, writing data to file...')
            break
        if((settings['Nb'] != -1) and (batch_num >= settings['Nb'])):
            break
        #end try
    #end while
    pool.close()
    pool.join()
    if(success == 1):
        print('\nLocation routine completed, starting tracking.')
        writers.writeProcessLogFooter(output_folder)
        # Generate raw tracks
        locations_fname = output_folder + "/locations.csv"
        all_locations = np.genfromtxt(locations_fname, delimiter=',')
        start_t = time.time()
        all_locations = all_locations[np.argsort(all_locations[:,3])]
        trackinglib.genTracks(all_locations, settings['search_d'], settings['max_skips'], settings['min_entries'], settings['extrap_s'], output_folder+'/tracks_raw.dsv')
        end_t = time.time()
        print('Completed tracking in ' + str(format(end_t-start_t, '.5f')) + 's.')
    
        # Stitch the tracks together
        tracks = np.genfromtxt(output_folder+'/tracks_raw.dsv', skip_header=1)
        #tracks = tracks_all[np.where(tracks_all[:,]),:]
        #print(tracks.shape)
        print("%d tracks located\n" % (len(np.unique(tracks[:,4]))))
        start_t = time.time()
        trackinglib.stitchTracks(tracks, settings['tol'], settings['search_d'], settings['extrap_s'], output_folder+'/tracks_stitched.dsv')
        end_t = time.time()
        print('Stitched in ' + str(format(end_t-start_t, '.5f')) + 's.\n')
    
        # Remove instances where tracer occurs in different places at the same
        # time
        stitched = np.genfromtxt(output_folder + '/tracks_stitched.dsv', skip_header=1)
        print("%d tracks located" % (len(np.unique(stitched[:,4]))))
        stitched = stitched[np.argsort(stitched[:,4])]
        dups_removed = np.zeros((0,6))
    
        for tid in np.unique(stitched[:,4]):
            inds = (np.where(stitched[:,4] == tid))[0]
            t = stitched[inds,3]
            values = stitched[inds,:]
            t_unique, indices, counts = np.unique(t, return_inverse=True, return_counts=True)
    
            rm = np.zeros((t_unique.shape[0], values.shape[1]))
            np.add.at(rm, indices, values)
            rm /= counts[:, newaxis]
            dups_removed = np.append(dups_removed, rm, axis=0)
    
        #os.remove(output_folder+'/tracks.csv')
        #os.remove(output_folder+'/stitched.csv')
    
        # Final output format is [t, x, y, z, ID, c]
        outfmt = '%10.1f %6.1f %6.1f %6.1f %-6i %-6i'
        np.savetxt(output_folder + '/tracks_cleaned.dsv', dups_removed, fmt=outfmt, delimiter='\t')

#end main

#ifndef COLLECTION_H
#define COLLECTION_H

#include <stdio.h>
#include "track_h.h"

/** @brief A structure containing a number of tracks.
 *
 * This structure contains some details to keep track of a number of Tracks.
 * Its members include a pointer to the first Track in a linked list of Tracks;
 * the number of Tracks currently in the list; and an ID counter used to assign
 * unique ID numbers to Tracks.
 */
struct Set_s
{
    Track* head;            //<! Pointer to the first track in the set.
    int num_tracks;         //<! Total number of tracks in the set.
    int id_counter;         //<! Counter used to assign IDs to tracks.
};
typedef struct Set_s Set;  

/** @brief Creates an empty Set.
 *
 * Creates a Set structure using dynamic memory allocation. The head pointer is
 * initialized to NULL, and the other values to 0.
 *
 * @return A pointer to the new Set.
 */
Set* createSet(void);
/** @brief Adds a new Track to a set.
 * 
 * Creates a new Track and adds it to the linked list of Tracks in the 
 * specified Set. The id_counter and num_tracks members are incremented.
 *
 * @param set       The set to which the Track will be added.
 * @param x,y,z     The initial spatial position of the Track.
 * @param t         The time at which the Track starts.
 * @param d			The size of the cluster used to determine the location
 * 
 * @return A pointer to the new Set.
 */
Set* addTrackToSet(Set* set, double x, double y, double z, double t, unsigned int d);
/** @brief Removes all Tracks flagged as terminated.
 *
 * This function traverses the linked list of Tracks, deleting all that have
 * the 'is_terminated' member set to 1.
 *
 * @param set A pointer to the Set from which Tracks must be deleted.
 *
 * @return A pointer to the updated set.
 */
Set* removeTerminated(Set* set);
/** @brief Disposes each Track in a Set
 *
 * This function traverses the linked list of Tracks in a Set and disposes of
 * each Track while freeing the memory.
 *
 * @param set A pointer to the set to be disposed.
 */
void disposeSet(Set* set);
              
Set* createSet(void)
{
    Set* new_set = (Set*) malloc(sizeof(Set));
    new_set->head = NULL;
    new_set->num_tracks = 0;
    new_set->id_counter = 0;
    return new_set;
}

Set* addTrackToSet(Set* set, double x, double y, double z, double t, unsigned int d)
{
    set->id_counter++;
    Track* new_track = createTrack(set->id_counter, set->head);
    new_track = addEntryToTrack(new_track, x, y, z, t, d);
    set->head = new_track;
    set->num_tracks++;
    return set;
}

Track* getTrackAt(Set* set, int index)
{
    Track* i_track = set->head;
    int counter = 0;
    while(counter < index) {
        if(i_track == NULL)
            return 0;
        i_track = i_track->next;
        counter++;
    }   
    
    return i_track;
}
    
Set* removeTerminated(Set* set)
{
    Track *cursor, *prev;
    cursor = set->head;
    prev = NULL;
    
    while(cursor != NULL) {
        if(cursor->is_terminated) {
            if(cursor == set->head) {
                set->head = cursor->next;
                disposeTrack(cursor);
                free(cursor);
                cursor = set->head;
            }
            else {
                prev->next = cursor->next;
                disposeTrack(cursor);
                free(cursor);
                cursor = prev->next;
            }
            set->num_tracks--;
        } else {
            prev = cursor;
            cursor = prev->next;
        }        
    }
    return set;
}
    
void disposeSet(Set* set)
{
    Track *cursor, *tmp;
    cursor = set->head;
    while(cursor != NULL) {
        tmp = cursor->next;
        disposeTrack(cursor);
        free(cursor);
        cursor = tmp;
        set->num_tracks--;
    }       
}


#endif

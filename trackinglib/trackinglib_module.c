/**
 * @file trackinglib_module.c
 
 * @author Dylan Blakemore
 
 * @brief File containing C functions to track tracer locations.
 *
 *
 * This C file contains structures and functions used to create tracks from 
 * multiple position-time data points. The implementation is a simplified
 * multiple target tracking technique.
 *
 * The code is compiled and wrapped using the Python-C API, and is used by the 
 * Voronoi-based Multiple Particle Tracking (VMPT) Python code. This routine
 * is written in C because the for-loops cannot be vectorised, and would 
 * therefore be inefficient to run in the Python interpreter.
 */
#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>
#include <stdio.h>
#include "entry_h.h"
#include "track_h.h"
#include "set_h.h"
#include "stitch_h.h"
/** @brief Matches Entries to Tracks.
 *
 * This function matches Entry structs created from a data set to the Tracks
 * in a Set. The position-time data is stored in an array of doubles. The data 
 * is indexed with data[i*4 + d], where i < n_new is the i-th new entry and
 * 0 <= d <= 3 is the dimension, with d = 0,1,2,3 representing x,y,z,t 
 * respectively.
 *
 * @param set       A pointer to the Set containing the existing Tracks.
 * @param data      A pointer to an array of double representing the position-time
 *                  data.
 * @param n_new     The number of new entries.
 * @param max_dist  The maximum radius within which to search for matches.
 * @param N         The number of entries to use for extrapolation.
 */
static Set* matchLocationsToTracks(Set* set, double* data, int n_new, double max_dist, int N);
                                  
/** @brief Determines tracks from a set of position-time data and writes to file.
 *
 * This function is the main function to be wrapped by the Python-C API. It
 * is the main entry point for the Python code.
 *
 * The function is a somewhat simplified implementation of a multiple target
 * tracking routine. It iteratively extracts all data points with the same time
 * stamp and either:
 *      - Tries to match the new data points to existing tracks, based on the 
 *        extrapolated locations of those tracks, or
 *      - Creates new tracks from data points which have not been associated
 *        with existing tracks.
 *
 * Once the tracks have been determined, they are written to a single .CSV
 * file with rows having the format (ID, x, y, z, t) where ID is the unique ID 
 * of the track, and (x, y, z) is the location of a tracer at some time t.
 *
 * Since the code is wrapped in Python, only two actual parameters are ever
 * passed: PyObject* self and PyObject* args.
 *      - self is a reference to the Python object calling the function, and
 *        is unused by the C function.
 *      - args is a pointer to an array of PyObjects which represent the paramaters
 *        passed to the function within the Python routine. In this case, there
 *        are 5 parameters:
 *          1. locations: An \f$N\times 4\f$ NumPy array. This contains the 
 *             \f$(x,y,z,t)\f$ position data of the located tracers.
 *          2. search_radius, the distance within which to match entries to tracks.
 *          3. max_skips: The maximum number of consecutive skipped entries
 *             before a track is terminated.
 *          4. min_entries: The minimum number of entries for a track to be
 *             considered valid for writing to disk.
 *          5. filepath: The path to the file to which the tracks are written.
 *
 * For more information, see the documentation for the main Python code.
 *
 * @param[in] self A pointer to the Python object which calls the function.
 * @param[in] args The arguments sent by Python to the function.
 *
 * @return An empty Python string. The tracks are written to file for further
 *         post processing as needed.
 */                                
static PyObject* trackinglib_genTracks(PyObject* self, PyObject* args)
{
    // t_rows = total number of positions.
    // i_row  = position that is currently being examined.
    int MAX_SKIPS, MIN_ENTRIES, EXTRAP_S;
    int t_rows, i_row;
    
    double SEARCH_RADIUS;
    double timestamp, tmp_t;
    double* location_data;
    
    char* filepath;
    
    PyArrayObject* locations;
    Set* set = createSet();
    Track* i_track;
                
    if (!PyArg_ParseTuple(args, "O!diiis", &PyArray_Type, &locations, 
                          &SEARCH_RADIUS, &MAX_SKIPS, &MIN_ENTRIES, &EXTRAP_S,
                          &filepath))
        return NULL;
                               
    // Create the output file
    FILE* fp;

    if((fp = fopen(filepath, "w"))) {
        fprintf(fp, "x\ty\tz\tt\tID\tc\n");
    } else {
        return NULL;
    }
    fclose(fp);
          
    location_data = (double*)PyArray_DATA(locations);
    t_rows = ((int)PyArray_Size(locations))/5;
    i_row  = 0;
    
    timestamp = location_data[3];
    tmp_t = timestamp;
    
    while(i_row < t_rows) {
        // The indices pointing to the start and end of the current frame.
        int f_start = i_row;
        int f_end   = i_row;   
        while(tmp_t == timestamp) {
            if(i_row >= t_rows)break;
            tmp_t = location_data[i_row*5 + 3];
            if(tmp_t == timestamp) {
                f_end++;
                i_row++;
            }
        }
        timestamp = tmp_t;
        int f_size = f_end - f_start;
        
        set = matchLocationsToTracks(set, location_data+(f_start*5),
                                     f_size, SEARCH_RADIUS, EXTRAP_S);
                                     
        // Write terminated tracks and mark for deletion.
        i_track = set->head;
        while(i_track != NULL) {
            if(i_track->num_skips > MAX_SKIPS) {
                if(i_track->num_entries >= MIN_ENTRIES)
                    writeTrackToFile(i_track, filepath);
                i_track->is_terminated = 1;
            }
            i_track = i_track->next;
        }
        
        set = removeTerminated(set);
    }
    i_track = set->head;
    while(i_track != NULL) {
        if(i_track->num_entries >= MIN_ENTRIES) {
            writeTrackToFile(i_track, filepath);
        }
        i_track = i_track->next;
    }
    
    disposeSet(set);                                        
    return Py_BuildValue("");
}


static Set* matchLocationsToTracks(Set* set, double* data, int n_new, double max_dist, int N)
{
    // Calculate the distance matrix.
    int i;
    Track* i_track;
    double new_t = data[3];
    int n_tracks = set->num_tracks;
    if(n_tracks == 0) {
        for(int j = 0; j < n_new; j++) {
            double x = data[j*5 + 0];
            double y = data[j*5 + 1];
            double z = data[j*5 + 2];
            double t = data[j*5 + 3];
            unsigned int d = (unsigned int)data[j*5 + 4];
            addTrackToSet(set, x,y,z,t,d);
        } 
        return set;
    }
    double* dists = NULL;
    dists = (double*) malloc(n_new*n_tracks*sizeof(double));
    if(dists == NULL) 
        return set;

    i = 0;
    i_track = set->head;
    while(i_track != NULL) {
        Entry* extrap = extendTrackForwards(i_track, new_t, N);
        for(int j = 0; j < n_new; j++) {
            double x = data[j*5 + 0];
            double y = data[j*5 + 1];
            double z = data[j*5 + 2];
            double t = data[j*5 + 3];
            unsigned int d = (unsigned int)data[j*5 + 4];
            Entry* j_entry = createEntry(x,y,z,t,d, NULL, NULL);
            dists[i*n_new + j] = getSquareDistance(extrap, j_entry);
            free(j_entry);
        }
        free(extrap);
        i_track = i_track->next;
        i++;
    }
    // Smallest distance per row   
    int* A = NULL;
    int* B = NULL;
    A = (int*) calloc(n_new*n_tracks, sizeof(int));
    // Smallest distance per column
    B = (int*) calloc(n_new*n_tracks, sizeof(int));
    
    // Fill A_ij with:
    //      1 if A_ij = min(A_i*)
    //      0 otherwise
    for(i = 0; i < n_tracks; i++) {
        int min_ind = 0;
        float min_val = dists[i*n_new + min_ind];
        for(int j = 1; j < n_new; j++) {
            double j_val = dists[i*n_new + j];
            if(j_val < min_val) {
                min_val = j_val;
                min_ind = j;
            }
        }
        A[i*n_new + min_ind] = 1;
    }
    // Fill B_ij with:
    //      1 if B_ij = min(A_*j)
    //      0 otherwise
    for(int j = 0; j < n_new; j++) {
        int min_ind = 0;
        double min_val = dists[min_ind*n_new + j];
        for(i = 1; i < n_tracks; i++) {
            double i_val = dists[i*n_new + j];
            if(i_val < min_val) {
                min_val = i_val;
                min_ind = i;
            }
        }
        B[min_ind*n_new + j] = 1;
    } 
            
    // Add matching locations to tracks.
    i = 0;
    i_track = set->head;
    int matchcounter = 0;
    while(i_track != NULL) {
        int matched = 0;
        for(int j = 0; j < n_new; j++) {
            int index = i*n_new + j;
            if(A[index]*B[index] == 1 && 
               dists[index] < max_dist*max_dist) { // Match
                double x = data[j*5 + 0];
                double y = data[j*5 + 1];
                double z = data[j*5 + 2];
                double t = data[j*5 + 3];
                unsigned int d = (unsigned int)data[j*5 + 4];
                addEntryToTrack(i_track, x, y, z, t, d);
                matched = 1;
                matchcounter++;
                break;
            }
        }  
        if(!matched) {
            i_track->num_skips++;
        }
        i_track = i_track->next;
        i++;
    }
    // Create new tracks from unmatched locations.
    for(int j = 0; j < n_new; j++) {
        int matched = 0;
        for(i = 0; i < n_tracks; i++) {
            int index = i*n_new + j;
            
            if(A[index]*B[index] == 1 && 
               dists[index] < max_dist*max_dist) { 
                matched = 1;
                break;
            }
        }        
        if(!matched) {
            double x = data[j*5 + 0];
            double y = data[j*5 + 1];
            double z = data[j*5 + 2];
            double t = data[j*5 + 3];
            unsigned int d = (unsigned int)data[j*5 + 4];
            addTrackToSet(set, x,y,z,t,d);
        }
    }
    free(A);
    free(B);
    free(dists);
    return set;
}

/** Stitch broken tracks together
 *
 */
static PyObject* trackinglib_stitchTracks(PyObject* self, PyObject* args)
{
    PyArrayObject* py_tracks;
    double tol, search;
    int interp_s, num_rows, cur_track, r;
    char* filepath;
    double* track_data;
    Set* set = createSet();
    
    if (!PyArg_ParseTuple(args, "O!ddis", &PyArray_Type, &py_tracks, &tol, &search, &interp_s, &filepath))
        return NULL;
        
    FILE* fp;
    if((fp = fopen(filepath, "w"))) {
        fprintf(fp, "x\ty\tz\tt\tID\tc\n");
    } else {
        return NULL;
    }
    fclose(fp);
    
    num_rows = ((int)PyArray_Size(py_tracks))/6;
    track_data = (double*)PyArray_DATA(py_tracks);
    // Create the set, initializing with all tracks
    r = 0;
    cur_track = -1;
    while(r < num_rows) {
        double x = track_data[r*6 + 0];
        double y = track_data[r*6 + 1];
        double z = track_data[r*6 + 2];
        double t = track_data[r*6 + 3];
        unsigned int d = (unsigned int)track_data[r*6 + 5];
        if((int)track_data[r*5] == cur_track) {
            addEntryToTrack(set->head, x, y, z, t, d);
        }
        else {
            cur_track = (int)track_data[r*6+4];  
            addTrackToSet(set, x, y, z, t, d);
        }  
        r++;        
    }
        
    set = stitchSet(set, tol, interp_s, search);
    Track* i_track = set->head;
    while(i_track != NULL) {
        writeTrackToFile(i_track, filepath);
        i_track = i_track->next;
    }
    
    disposeSet(set);
    return Py_BuildValue("");        
}
/**
    Functions used during the compiling process to create the library
**/
/* Documentation strings */
static char module_docstring[] =
    "This module uses the location output to generate tracks.";
static char genTracks_docstring[] = 
    "Returns the batch of frames specified by a starting point.";
static char stitchTracks_docstring[] = 
    "Stitches broken tracks together.";
    
/* Add all methods */
static PyMethodDef trackinglib_methods[] = {
    {"genTracks", trackinglib_genTracks, METH_VARARGS, genTracks_docstring},
    {"stitchTracks", trackinglib_stitchTracks, METH_VARARGS, stitchTracks_docstring},
    {NULL, NULL, 0, NULL}
};

/* Entry point for Python script */
PyMODINIT_FUNC inittrackinglib(void) {
    PyObject *m = Py_InitModule3("trackinglib", 
                    trackinglib_methods,
                   module_docstring);
    if(m == NULL)
        return;

    import_array();
}
        

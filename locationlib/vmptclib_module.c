#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>
#include <stdio.h>

static int getListItemInt(PyObject* py_list, int index) {
    if(PyList_Check(py_list)) {
        return (int)PyInt_AsSsize_t(PyList_GetItem(py_list, index));
    } else {
        return -1;
    }
}

static int getMatrixRow(double* data, int row, int dims, double* entry) {
    for(int i = 0; i < dims; i++) {
        int index = row * dims + i;
        entry[i] = data[index];
    }
    return 0;
}

static double getEuclideanDistance(double* A, double* B) {
    double sqsum = 0;    
    for(int i = 0; i < 3; i++) {
        sqsum += pow(A[i] - B[i],2);
    }
    return sqrt(sqsum);
}

static PyObject* vmptclib_getSmallestRegion(PyObject* self, PyObject* args) {
    int num_lines;                  // the total number of LOR's in the frame
    PyObject* line_indices;         // (npoints,1) list of ints; ID's of lines along which points lie
    PyObject* point_regions;        // (npoints,1) list of ints; indices into Voronoi regions
    PyObject* regions;              // (nregions,*) list of list of ints; indices into vertices
    PyArrayObject* vertices;        // (nvertices,3) ndarray of doubles; co-ords of vertices
    /* variables used throughout the function */
    int line_counter    = 0;
    int point_counter   = 0;
    double min_volume   = 100000.0;
    int num_points;
    int line_id;
    /* Verify argument data types */
    if (!PyArg_ParseTuple(args, "iOOOO!", &num_lines, 
                                         &line_indices, 
                                         &point_regions,
                                         &regions,
                                         &PyArray_Type, &vertices))
        return NULL;
    
    double* vertex_data = (double*)PyArray_DATA(vertices);
    num_points = (int)PyList_Size(line_indices);    // the total number of seed points
    PyObject* output_indices = PyList_New(num_lines);
    PyObject* output_volumes = PyList_New(num_lines);

    while(point_counter < num_points) {
        line_id = getListItemInt(line_indices, point_counter);
        if(line_id == line_counter) {
            
            int region_index        = getListItemInt(point_regions, point_counter);    // the index of the Voronoi region
            PyObject* vertex_list   = PyList_GetItem(regions, region_index);      // a list of the  vertices
            int num_vertices        = (int)PyList_Size(vertex_list);                   // total number of vertices     
            double centroid[3]      = {0.0, 0.0, 0.0};
            
            for(int vertex_i = 0; vertex_i < num_vertices; vertex_i++) {
                int vertex_row = getListItemInt(vertex_list, vertex_i);         // vertex entry in vertices object
                if(vertex_row != -1) {
                    double vertex[3];
                    getMatrixRow(vertex_data, vertex_row, 3, vertex);
                    for(int dim = 0; dim < 3; dim++)
                        centroid[dim] = centroid[dim] + vertex[dim] / (float)num_vertices;
                }          
            }
             
            double region_volume    = 0;   
            for(int vertex_i = 0; vertex_i < num_vertices; vertex_i++) {
                int vertex_row = getListItemInt(vertex_list, vertex_i);         // vertex entry in vertices object
                double vertex[3];     
                if(vertex_row != -1) {           
                    getMatrixRow(vertex_data, vertex_row, 3, vertex);   
                    double distance = getEuclideanDistance(vertex, centroid);
                    region_volume = region_volume +  distance/num_vertices;
                
                } else {
                    region_volume = 100000.0;
                    break;                
                }
            }
            
            if( region_volume <= min_volume) {
                PyObject* py_index = Py_BuildValue("i",point_counter);
                PyObject* py_volume = Py_BuildValue("f", region_volume);
                PyList_SetItem(output_indices, line_id, py_index);
                PyList_SetItem(output_volumes, line_id, py_volume);
                min_volume = region_volume;
            }
        
            point_counter++;
        } else {
            min_volume = 100000.0;
            line_counter++;
        }
    }
    //free(vertex_data);
    return Py_BuildValue("OO",output_indices,output_volumes);
}
/*=============================================================================
 *============================================================================= 
 *=============================================================================
 *=============================================================================
 *===========================================================================*/
 
const static float pi=3.14157, rad=420, dring=38.8, dplane=4.85; //<! constants used in the decoding.

/** @brief Decodes a single word from the list mode file.
 *
 * The information in a list mode file is stored with a custom compression.
 * This function decodes a single word (consisting of 4 chars) into two points
 * representing two detections which create a line of response when joined.
 *
 * @param[in] word An array of 4 chars read from the list mode file.
 * @param[out] positions A pointer to the first in an array of floats storing 
 *                       the positions of the two end points of a line of 
 *                       response. The array takes the form 
 *                       (Xa, Ya, Za, Xb, Yb, Zb).
 */
static void decodeWord(const unsigned char* word, float* positions)
{
    unsigned int longword = word[3]+256*word[2]+256*256*word[1]+256*256*256*word[0];
                                
    int sangle = longword & 511;
	int offset = (longword>>9) & 511;
	if(offset>255)offset = offset - 512;
	int ringA = (longword>>25) & 7;
	int ringB = (longword>>28) & 7;
	int planeA = (longword>>19) & 7;
	int planeB = (longword>>22) & 7;
	int i = abs(offset) % 2;
	int detA = (576 + (offset-i)/2 + sangle)%576;
	int detB = (288-(offset+i)/2 + sangle)%576;

	float thetaA = pi * detA/288;
	float thetaB = pi * detB/288;

    positions[0] = rad*sin(thetaA);
    positions[1] = rad*cos(thetaA);
    positions[2] = planeA*dplane+(ringA-3)*dring;
    positions[3] = rad*sin(thetaB);                    
    positions[4] = rad*cos(thetaB);
    positions[5] = planeB*dplane+(ringB-3)*dring;
} 

/** @brief Decodes a batch of list mode data into usable positions.
 *
 * This function is wrapped by the Python-C API and called from the Python 
 * script. The function opens a list mode file (.lm format, generated by the 
 * PET camera) and returns a number of lines of response, with each LOR 
 * consisting of two end points and a time stamp.
 *
 * It does this using the following steps:
 *  - Move the pointer in the file to the desired starting point. The starting
 *    point is the index to the \f$i\f$th line of response, so time increments
 *    and delayed coincidences are skipped.
 *  - Decode the specified number of LOR's and add them to a NumPy array object.
 *
 * Although the C function only accepts two parameters (self and args), the
 * args variable is a pointer to a PyObject which contains the parameters sent
 * to the Python function.
 *
 * @param self A pointer to the Python object which calls the function.
 * @param args A pointer to a Python object which stores the parameters sent 
 *             by the Python script. It contains the following parameters:
 *              - filename: The path to the list mode file.
 *              - start_line: The index of the LOR at which to start decoding.
 *              - batch_size: The total number of LOR's to decode.
 *
 * @return -1 if the file cannot be opened; -2 if the desired start line is
 *         larger than the number of lines in the file; otherwise a NumPy array
 *         with dimensions \f$N\times 7\f$ with each line having the format
 *         \f$(Xa, Ya, Za, Xb, Yb, Zb, t)\f$ where \f$(Xa, Ya, Za)\f$ and 
 *         \f$(Xb, Yb, Zb)\f$ are the two end points of a line of response 
 *         detected at time \f$t\f$.
 */
static PyObject* vmptclib_getFrameBatch(PyObject* self, PyObject* args)
{
    FILE* input; //<! The input list mode file.
    // The object to return.
    //PyObject* batch = PyList_New(init_size);
    // The arguments passed as parameters to the function.
    const char *filename;
    long int start_byte;
    int frame_size;
    int num_frames;
    int window_size;
    int batch_size;
    int index = 0;
    int tlines = 0;
    float time;
    // Parse the args object to the specified parameters.
    if (!PyArg_ParseTuple(args, "slfiii", &filename, &start_byte, &time, &frame_size, &num_frames, &window_size))
        return NULL;
    batch_size = num_frames*frame_size; 
    if(window_size <= 0 || window_size > frame_size)window_size=frame_size;
    PyObject* batch = PyList_New(batch_size*7);
    // Open the file to the starting position.
    input = fopen(filename, "rb");
    // Returns an array of length 1 with the value -1 if the file can't be 
    // opened.
    if(input == 0) {
        return Py_BuildValue("ilfO", -1, start_byte, time, batch);
    }
    // Seeks to the correct position. If the position is greater than the end
    // of the file, return -2.
    if(fseek(input, start_byte, SEEK_SET)) {
        return Py_BuildValue("ilfO", -2, start_byte, time, batch);
    }
    // Read the words for the frame and decode them
    unsigned int idum;
    unsigned char word[4];
    long int jump_byte = start_byte;
    int jump_time = time;
    int eof = 0;
    
    for(int iframe = 0; iframe < num_frames; iframe++) {
        int iline = 0;
        while(iline < frame_size) {
            // end of file
            if(feof(input)) {
                eof = 1;
                break;
            }
            for(idum = 0; idum < 4; idum++)word[idum] = fgetc(input);
            start_byte += 4;
            // increment time
            if(word[0]&128) {
                time++;
                continue;
            }
            // delayed coincidence - discard
            if(word[1]&4)continue;
            float positions[6];
            // Decode the word
            decodeWord(word, positions);
            // Add each float in the position array to the output NumPy array.
            for(unsigned int i = 0; i < 6; i++) {
                //PyList_Append(batch, Py_BuildValue("f",positions[i]));
                PyObject* val = Py_BuildValue("f",positions[i]);
                PyList_SetItem(batch, index, val);
                index++;
            }
                
            //PyList_Append(batch, Py_BuildValue("f",time));
            PyObject* val = Py_BuildValue("f",time);
            PyList_SetItem(batch, index, val);
            index++;
            iline++;
            tlines++;
            if(iline == window_size) {
                jump_byte = start_byte;
                jump_time = time;
            }
        }
        if(eof)break;
        
        if(fseek(input, jump_byte, SEEK_SET)) {
            return Py_BuildValue("ilfO", -2, start_byte, time, batch);
        } else {
            start_byte = jump_byte;   
            time = jump_time;
        }
    }
//=============================================================================
//     while(iline < batch_size) {
//         // end of file
//         if(feof(input))break;
//         for(idum = 0; idum < 4; idum++)word[idum] = fgetc(input);
//         start_byte += 4;
//         // increment time
//         if(word[0]&128) {
//             time++;
//             continue;
//         }
//         // delayed coincidence - discard
//         if(word[1]&4)continue;
//         float positions[6];
//         // Decode the word
//         decodeWord(word, positions);
//         // Add each float in the position array to the output NumPy array.
//         for(unsigned int i = 0; i < 6; i++) {
//             //PyList_Append(batch, Py_BuildValue("f",positions[i]));
//             PyObject* val = Py_BuildValue("f",positions[i]);
//             PyList_SetItem(batch, index, val);
//             index++;
//         }
//             
//         //PyList_Append(batch, Py_BuildValue("f",time));
//         PyObject* val = Py_BuildValue("f",time);
//         PyList_SetItem(batch, index, val);
//         index++;
//         iline++;
//     }
//=============================================================================
    
    fclose(input);
    return Py_BuildValue("ilfO", tlines, start_byte, time, batch);
}
/*=============================================================================
 *============================================================================= 
 *=============================================================================
 *=============================================================================
 *===========================================================================*/

/* @brief Returns the index of the starting point in the file based on a time.
 *
 */     
static PyObject* vmptclib_getStartingPoint(PyObject* self, PyObject* args)
{
    FILE* input; //<! The input list mode file.
    const char *filename;
    int start_time;
    if (!PyArg_ParseTuple(args, "si", &filename, &start_time))
        return NULL;  
    // Open the file to the starting position.
    fpos_t frame_start;
    input = fopen(filename, "rb");
    // Returns an array of length 1 with the value -1 if the file can't be 
    // opened.
    if(input == 0) { 
        return Py_BuildValue("i", -1);
    }
    fgetpos(input, &frame_start);
    
    int tflag = 1;
    unsigned int idum;
    unsigned char word[4];
    long int cur_byte = 0;
    float time = 0;
    // Loop through the data file until the desired starting point is reached.
    while(tflag) {
        // exit the loop when start time is reached
        if(time >= start_time) {
            tflag = 0;
            break;
        }
        for(idum = 0; idum < 4; idum++)word[idum] = fgetc(input);
        // If the end of file is reached before the desired starting point, 
        // return -2.
        if(feof(input)) {
            fclose(input);
            return Py_BuildValue("i", -2);        
        }
        cur_byte += 4; 
        // delayed coincidence - discard
        if(word[1]&4)continue;
        // increment time
        if(word[0]&128){
            time++;
            continue;
        }
    }
    return Py_BuildValue("lf", cur_byte, time);
}
        
/* Documentation strings */
static char module_docstring[] =
    "This module provides methods written in C for the VMPT program.";
static char getSmallestRegion_docstring[] = 
    "Get the smallest Voronoi region per line of response."; 
static char getFrameBatch_docstring[] = 
    "Get a batch of frames.";   
static char getStartingPoint_docstring[] = 
    "Returns the line index that corresponds to some starting time.";

/* Add all methods */
static PyMethodDef vmptclib_methods[] = {
    {"getSmallestRegion", vmptclib_getSmallestRegion, METH_VARARGS, getSmallestRegion_docstring},
    {"getFrameBatch", vmptclib_getFrameBatch, METH_VARARGS, getFrameBatch_docstring},
    {"getStartingPoint", vmptclib_getStartingPoint, METH_VARARGS, getStartingPoint_docstring},
    {NULL, NULL, 0, NULL}
};

/* Entry point for Python script */
PyMODINIT_FUNC initvmptclib(void) {
    PyObject *m = Py_InitModule3("vmptclib", 
                    vmptclib_methods,
                   module_docstring);
    if(m == NULL)
        return;

    import_array();
}
import numpy as np
import os.path
from ConfigParser import SafeConfigParser
    
def getLowFraction(data, fraction):
    sorted_data = list(data)
    sorted_data.sort()
    largest_valid = sorted_data[int(len(data) * fraction)]
    
    indices = np.argwhere(data <= largest_valid)
        
    return indices[:,0]
    
def writeLocationsToFile(output_folder, location_output):
    stripped_location = location_output[~np.all(location_output == 0, axis=1)] # removes trailing rows of zeros
    output_fname = output_folder + "/locations.csv"
    
    if os.path.isfile(output_fname):
        f_handle = open(output_fname, 'a')
        np.savetxt(f_handle, stripped_location, delimiter=',')
        f_handle.close()
    else:
        np.savetxt(output_fname, stripped_location, delimiter=',')
    
def printProgress(file_num, progress, average_tracers):
    print('-')
    print("File " + str(file_num + 1) + " progress: " + str(progress) + "%")
    print("Average number of tracers per frame: %.2f" % average_tracers)
    
def loadSettings(config_path):
    config = SafeConfigParser()
    config.read(config_path)
    settings = {};
    # Variables extracted from the configuration file.
    settings['lpt']             = config.getint('Input','LinesPerTracer')
    settings['fpb']             = config.getint('Input','FramesPerBatch')
    settings['Nb']              = config.getint('Input','NumBatches')
    settings['t0']              = config.getint('Input', 'StartTime')
    settings['eps']             = config.getfloat('Processing','Eps')
    settings['k']               = config.getint('Processing','K')
    settings['lof_frac']        = config.getfloat('Processing','LofFrac')
    settings['vol_frac']        = config.getfloat('Processing','VolFrac')
    settings['cores']           = config.getint('Machine','NumCores')
    settings['min_entries']     = config.getint('Tracking','MinEntries')
    settings['max_skips']       = config.getint('Tracking','MaxSkips')
    settings['search_d']        = config.getfloat('Tracking','SearchDist')
    settings['extrap_s']        = config.getint('Tracking','ExtrapS')
    settings['min_rate']        = config.getfloat('Processing','MinRate')
    settings['window']          = config.getint('Processing','Window')
    settings['tol']             = config.getint('Tracking','Tolerance')

    file_path        = config.get('Input','InFile')
    output_folder    = config.get('Input','OutFolder')
    settings['Nt']   = config.getint('Input','Ntracers')
    
    return [settings, file_path, output_folder]
    


# -*- coding: utf-8 -*-
import datetime

def writeLogHeader(settings, outfolder):
    logfile = open(outfolder + '/log.txt', 'a')
    logfile.write('====================================================================\n')
    logfile.write('N_tracers=' + str(settings['Nt']) + '\n')
    logfile.write('N_cores=' + str(settings['cores']) + '\n')
    logfile.write('nlines=' + str(settings['lpt']) + '; eps=' + str(settings['eps']) + '; k=' + str(settings['k']) + '\n')
    logfile.write('Location Start = ' + datetime.datetime.now().strftime('%I:%M%p on %B %d, %Y') + '\n')
    logfile.write('--------------------------------------------------------------------\n')
    logfile.write('Tracers detected \t Time taken \t Percent complete \t ETA \n')
    logfile.close()
    
## @brief Writes the time to process the number of frames to the log file.
#         Called after a batch of frames has been processed.
#
# @param frames The number of frames processed.
# @param time The time taken to process the frames in seconds.
def writeProcessLog(ntdet, time, percent, eta, outfolder):
    logfile = open(outfolder + '/log.txt','a')
    logfile.write(str(ntdet) + '\t' + time + '\t' + percent + '\t' + eta + '\n')
    logfile.close()
# end writeLog() method

## @brief Writes the footer to the log file once all data has been processed.
def writeProcessLogFooter(outfolder):
    logfile = open(outfolder + '/log.txt', 'a')
    logfile.write('--------------------------------------------------------------------\n')
    logfile.write('Location End = ' + datetime.datetime.now().strftime('%I:%M%p on %B %d, %Y\n'))
    logfile.close()
# end writeLogFooter()

def writeTrackerHeader(outfolder):
    logfile = open(outfolder + '/log.txt', 'a')
    logfile.write('Tracking Start = ' + datetime.datetime.now().strftime('%I:%M%p on %B %d, %Y\n'))
    logfile.close()
# end writeTrackerHeader()

def writeTrackerFooter(outfolder):
    logfile = open(outfolder + '/log.txt', 'a')
    logfile.write('Tracking End = ' + datetime.datetime.now().strftime('%I:%M%p on %B %d, %Y\n'))
    logfile.close()
# end writeTrackFooter()
#ifndef TRACK_H
#define TRACK_H
#include "entry_h.h"

/** @brief A structure representing the path of a tracer through space.
 *
 * This struct represents a trajectory or track of an object through space and
 * time. The structure contains  pointers to the first and last nodes of a
 * doubly linked list of Entry structs. It also contains values for the total
 * number of entries in the linked list; the number of consecutive skipped
 * time steps; whether or not the track has been terminated; a unique ID; the
 * chronologically most recent entry time; and the chronologically first entry
 * time.
 *
 * An indeterminate number of tracks are created, so each Track is itself a 
 * node in a linked list of Tracks. The struct therefore contains a pointer to
 * the next Track in the list.
 */
struct Track_s
{
    Entry* entry_0;         //!< Pointer to the first entry.
    Entry* entry_n;         //!< Pointer to the last entry.
    int num_entries;        //!< Total number of entries in the track.
    int num_skips;          //!< Number of consecutive skips.
    int is_terminated;      //!< Whether the track has been terminated.
    int id;                 //!< Unique ID of the track.
    double t_0;             //!< Most recent time.
    double t_n;             //!< Least recent time.
    
    struct Track_s* next;
};
typedef struct Track_s Track;

/** @brief Creates a new Track.
 *
 * Creates a new Track structure using dynamic memory allocation.
 *
 * @param id    The unique ID of the track.
 * @param next  A pointer to the next Track in the linked list.
 *
 * @return      A pointer to the new Track.
 */
Track* createTrack(int id, Track* next);

/** @brief Adds an Entry to the Track.
 *
 * This function creates a new Entry from the input parameters and adds it
 * to the linked list of Entries representing the trajectory. Entries are added
 * chronologically. This is done by checking the new time against the time of 
 * the most recent Entry and the time of the most distant Entry. If the time
 * is less than the most distant, the Entry is added to the back of the list.
 * If it is larger than the most recent, it is added to the front of the list.
 * If the time is somewhere in between, the list is traversed until the correct
 * location is found.
 *
 * @param track A pointer to the Track to which the Entry will be added.
 * @param x,y,z The spatial coordinates of the new Entry.
 * @param t     The time at which the Entry was located.
 *
 * @return A pointer to the updated Track.
 */
Track* addEntryToTrack(Track* track, double x, double y, double z, double t, unsigned int d);
/** @brief Extrapolates the position of the tracer forwards in time.
 * 
 * This function uses the N most recent entries to predict the position
 * of the object at some time t_new. It does so by finding a line of best fit
 * for each of the x-, y-, and z-vs-t dimensions. 
 * 
 * @param track A pointer to the Track to extrapolate.
 * @param t_new The time at which the best fit lines are evaluated.
 * @param N     The number of Entries to use when fitting the lines.
 *
 * @return A pointer to an Entry representing the extrapolated position.
 */
Entry* extendTrackForwards(Track* track, double t_new, int N);
/** @brief Extrapolates the position of the tracer backwards in time.
 *
 * This function uses the first N entries of a track to predict the position
 * of the object at some time t_new. It does so by finding a line of best fit
 * for each of the x-, y-, and z-vs-t dimensions. 
 * 
 * @param track A pointer to the Track to extrapolate.
 * @param t_new The time at which the best fit lines are evaluated.
 * @param N     The number of Entries to use when fitting the lines.
 *
 * @return A pointer to an Entry representing the extrapolated position.
 */
Entry* extendTrackBackwards(Track* track, double t_new, int N);
/** @brief Disposes of the entries in a track. 
 *
 * This function iteratively frees the memory for each Entry in a given Track.
 * 
 * @param track A pointer to the Track to be disposed.
 */
void disposeTrack(Track* track);
/** @brief Writes the entries in a Track to file.
 *
 * Writes a Track, Entry by Entry, into the specified file. The Entries are
 * appended, and take the form [id, x, y, z, t] where [id] is the unique ID
 * of the track, [x, y, z] is the spatial position of the object, and [t] is
 * the time of that location.
 *
 * @param track A pointer to the Track to be written to file.
 * @param fname The path to the output file.
 */
int writeTrackToFile(Track* track, char* fname);
                      
Track* createTrack(int id, Track* next)
{
    Track* track = (Track*) malloc(sizeof(Track));
    track->id            = id;
    track->is_terminated = 0;
    track->num_entries   = 0;
    track->num_skips     = 0;
    track->t_0           = 0.0;
    track->t_n           = 0.0;
    track->entry_0       = NULL;
    track->entry_n       = NULL;
    track->next          = next;
                       
    return track;
}

Track* addEntryToTrack(Track* track, double x, double y, double z, double t, unsigned int d)
{
    Entry* new_entry; //= createEntry(x, y, z, t, track->entry_0);
    //new_entry->matched = track->id;
    if(track->num_entries == 0)
    {
        track->t_0 = t;
        track->t_n = t; 
        new_entry = createEntry(x, y, z, t, d, NULL, NULL);
        if(new_entry == NULL) {
            printf("Could not add entry\n");
            return 0;
        }
        new_entry->matched = track->id;
        track->entry_0   = new_entry;
        track->entry_n   = new_entry;
    } 
    else {
        // The entry is the most recent chronologically (has the largest t)
        if(t > track->t_0)
        {
            track->t_0 = t;
            new_entry = createEntry(x, y, z, t, d, track->entry_0, NULL);
            new_entry->matched = track->id;
            track->entry_0 = new_entry;                      
        }
        // The entry is the least recent (has the smallest t)
        else if(t < track->t_n) {
            track->t_n = t; 
            new_entry = createEntry(x, y, z, t, d, NULL, track->entry_n);
            new_entry->matched = track->id;
            track->entry_n->next = new_entry;
            track->entry_n = new_entry;
        }
        else {
            Entry* cursor = track->entry_0;
            while(t > cursor->t) {
                cursor = cursor->next;  
            }             
            new_entry = createEntry(x, y, z, t, d, cursor, cursor->prev);
            new_entry->matched = track->id;
            cursor->prev->next = new_entry;
            cursor->prev = new_entry;
        }
    }
    
    track->num_skips = 0;
    track->num_entries++;
    
    return track;
}
    
void disposeTrack(Track* track)
{
    Entry *cursor, *tmp, *head;
    head = track->entry_0;

    if(head != NULL) {
        cursor = head->next;
        head->next = NULL;
        while(cursor != NULL) {
            tmp = cursor->next;
            free(cursor);
            cursor = tmp;
        }
    }       
}
        
Entry* extendTrackForwards(Track* track, double t_new, int N)
{
    Entry* i_entry = NULL;
    int i;
    unsigned int d = track->entry_0->d;
    
    if(track->num_entries == 1) {
        double x = track->entry_0->x;   
        double y = track->entry_0->y;
        double z = track->entry_0->z;
        double t = track->entry_0->t;
        Entry* extrap = createEntry(x,y,z,t,d,NULL,NULL);
        return extrap;
    }
    int end = track->num_entries - 1;
    
    int min_N = end<N ? end:N;
    double x_av = 0;
    double y_av = 0;
    double z_av = 0;
    double t_av = 0;
    // Determine the average value of all variables
    i_entry = track->entry_0;
    i = 0;
    while(i < min_N) {
        x_av += i_entry->x;   
        y_av += i_entry->y; 
        z_av += i_entry->z; 
        t_av += i_entry->t; 
        i_entry = i_entry->next;
        i++;
    }
    x_av = x_av/min_N;
    y_av = y_av/min_N;
    z_av = z_av/min_N;
    t_av = t_av/min_N;
    
    // Determine the gradient of a best fit line for each dimension
    double m_x = 0;
    double m_y = 0;
    double m_z = 0;
    double t_sum_sq = 0;
    i_entry = track->entry_0;
    i = 0;
    while(i < min_N) {
        t_sum_sq += (i_entry->t - t_av)*(i_entry->t - t_av);   
        i_entry = i_entry->next;
        i++;
    }
    if(t_sum_sq == 0) {
        m_x = 0;
        m_y = 0;
        m_z = 0;        
    }
    else {
        i_entry = track->entry_0;
        i = 0;
        while(i < min_N){
            m_x += (i_entry->t - t_av)*(i_entry->x - x_av)/t_sum_sq; 
            m_y += (i_entry->t - t_av)*(i_entry->y - y_av)/t_sum_sq;   
            m_z += (i_entry->t - t_av)*(i_entry->z - z_av)/t_sum_sq;   
            i_entry = i_entry->next;
            i++;
        }
    }
    // Determine the intercept of the best fit line
    double x_int = x_av - m_x*t_av;
    double y_int = y_av - m_y*t_av;
    double z_int = z_av - m_z*t_av;
    
    // Plug the next time into the best fit line equations
    Entry* extrap = createEntry(x_int + m_x*t_new,
                                y_int + m_y*t_new,
                                z_int + m_z*t_new,
                                t_new, d, NULL, NULL);
    return extrap;         
}
        
Entry* extendTrackBackwards(Track* track, double t_new, int N)
{
    Entry* i_entry = NULL;
    int i;
    unsigned int d = track->entry_0->d;
    
    if(track->num_entries == 1) {
        double x = track->entry_n->x;   
        double y = track->entry_n->y;
        double z = track->entry_n->z;
        double t = track->entry_n->t;
        Entry* extrap = createEntry(x,y,z,t,d,NULL,NULL);
        return extrap;
    }
    int end = track->num_entries - 1;
    
    int min_N = end<N ? end:N;
    double x_av = 0;
    double y_av = 0;
    double z_av = 0;
    double t_av = 0;
    // Determine the average value of all variables
    i_entry = track->entry_n;
    i = 0;
    while(i < min_N) {
        x_av += i_entry->x;   
        y_av += i_entry->y; 
        z_av += i_entry->z; 
        t_av += i_entry->t; 
        i_entry = i_entry->prev;
        i++;
    }
    x_av = x_av/min_N;
    y_av = y_av/min_N;
    z_av = z_av/min_N;
    t_av = t_av/min_N;
    
    // Determine the gradient of a best fit line for each dimension
    double m_x = 0;
    double m_y = 0;
    double m_z = 0;
    double t_sum_sq = 0;
    i_entry = track->entry_n;
    i = 0;
    while(i < min_N) {
        t_sum_sq += (i_entry->t - t_av)*(i_entry->t - t_av);   
        i_entry = i_entry->prev;
        i++;
    }
    if(t_sum_sq == 0) {
        m_x = 0;
        m_y = 0;
        m_z = 0;        
    }
    else {
        i_entry = track->entry_n;
        i = 0;
        while(i < min_N){
            m_x += (i_entry->t - t_av)*(i_entry->x - x_av)/t_sum_sq; 
            m_y += (i_entry->t - t_av)*(i_entry->y - y_av)/t_sum_sq;   
            m_z += (i_entry->t - t_av)*(i_entry->z - z_av)/t_sum_sq;   
            i_entry = i_entry->prev;
            i++;
        }
    }
    // Determine the intercept of the best fit line
    double x_int = x_av - m_x*t_av;
    double y_int = y_av - m_y*t_av;
    double z_int = z_av - m_z*t_av;
    
    // Plug the next time into the best fit line equations
    Entry* extrap = createEntry(x_int + m_x*t_new,
                                y_int + m_y*t_new,
                                z_int + m_z*t_new,
                                t_new, d, NULL, NULL);
    return extrap;         
}
        
int writeTrackToFile(Track* track, char* path)
{
    FILE* fp;
    Entry* cursor = track->entry_0;

    if((fp = fopen(path, "a"))) {
        while(cursor != NULL) {
            double x = cursor->x;
            double y = cursor->y;
            double z = cursor->z;
            double t = cursor->t;
            unsigned int d = cursor->d;
            fprintf(fp, "%f\t%f\t%f\t%f\t%d\t%d\n", x, y, z, t, track->id, d);
            cursor = cursor->next;
        }   
    } else {
        return 0;
    }
    fclose(fp);
    return 1;
}
    
#endif

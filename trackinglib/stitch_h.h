#ifndef STITCH_H
#define STITCH_H

#include "set_h.h"
#include "track_h.h"
#include "entry_h.h"

Set* stitchSet(Set* orig, double tol, int interp_s, double search);   
                 
Set* stitchSet(Set* orig, double tol, int interp_s, double search)
{
    Track* A = orig->head;
    Track* B = A->next;
    while(A->next != NULL) {
        while(B != NULL) {
            int matched = 0;
            double mid_t = 0;
            Entry *extrap_A, *extrap_B;
            // A before B
            if(A->t_n < B->t_n && A->t_0 < B->t_0) {
                double overlap = A->t_0 - B->t_n;
                if(B->t_n > (A->t_0 - tol)) {
                    mid_t = (A->t_0 + B->t_n)/2.0;
                    extrap_A = extendTrackForwards(A, mid_t, interp_s);
                    extrap_B = extendTrackBackwards(B, mid_t, interp_s);
                    matched = 1;  
                }
            } 
            // B before A
            if(B->t_n < A->t_n && B->t_0 < A->t_0) {
                if(A->t_n > (B->t_0 - tol)) {
                    mid_t = (B->t_0 + A->t_n)/2.0;
                    extrap_B = extendTrackForwards(B, mid_t, interp_s);
                    extrap_A = extendTrackBackwards(A, mid_t, interp_s);
                    matched = 1;  
                }
            }
            if(matched) {
                double sqdist = getSquareDistance(extrap_A, extrap_B);
                if(sqdist < search*search) {
                    B->id = A->id;  
                }
                free(extrap_A);
                free(extrap_B);
            }
            B = B->next;
        }
        A = A->next;
        B = A->next;        
    }
    return orig;    
}

#endif
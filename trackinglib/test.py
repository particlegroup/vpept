import tracker
import numpy as np
import time

SEARCH_DIST     = 10
MAX_SKIPS       = 10
MIN_ENTRIES     = 50
EXTRAP_S        = 20

output_folder = 'input'

locations_fname = output_folder + '/locations.csv'
all_locations = np.genfromtxt(locations_fname, delimiter=',')
start_t = time.time()
# Sort locations by time.
all_locations = all_locations[np.argsort(all_locations[:,3])]
tracker.genTracks(all_locations, SEARCH_DIST, MAX_SKIPS, MIN_ENTRIES, EXTRAP_S, output_folder+'/tracks.csv')
end_t = time.time()
print('Completed tracking in ' + str(end_t-start_t) + 's.\n')

tracks = np.genfromtxt(output_folder+'/tracks.csv', delimiter=',', skip_header=1)
start_t = time.time()
tracker.stitchTracks(tracks, 100.0, SEARCH_DIST, EXTRAP_S, output_folder+'/stitched.csv')
end_t = time.time()
print('Stitched in ' +str(end_t - start_t) + 's.\n')